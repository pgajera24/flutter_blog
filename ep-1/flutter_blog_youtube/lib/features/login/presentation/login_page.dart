import 'package:flutter/material.dart';
import 'package:flutter_blog/stdlib/ui/colors.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: BlogColors.primary.gradient,
        ),
        child: Center(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 19.0),
          child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.black38,
                      offset: const Offset(3, 4),
                      spreadRadius: 3,
                      blurRadius: 3)
                ],
                borderRadius: const BorderRadius.all(Radius.circular(15)),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Log In",
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    _buildField("email", context,
                        secure: false, icon: Icons.email),
                    _buildField("password", context,
                        secure: true, icon: Icons.lock),
                    MaterialButton(
                      onPressed: () {},
                      color: Theme.of(context).primaryColor,
                      child: Text(
                        "Log In".toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                ),
              )),
        )),
      ),
    );
  }

  Widget _buildField(String text, BuildContext context,
      {bool secure = false, IconData icon}) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
        child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: TextField(
                obscureText: secure,
                cursorColor: Theme.of(context).cursorColor,
                decoration: InputDecoration(
                  prefixIcon: Icon(icon),
                  hintText: text,
                ))));
  }
}
