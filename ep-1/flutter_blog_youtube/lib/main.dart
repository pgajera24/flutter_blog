import 'package:flutter/material.dart';
import 'package:flutter_blog/stdlib/ui/colors.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Blog',
      theme: ThemeData(
        primaryColor: BlogColors.primary.color,
        accentColor: BlogColors.secondary.color,
        brightness: Brightness.light,
        cursorColor: BlogColors.primary.color,
        fontFamily: "Avenir Next",
        textTheme: TextTheme(
            headline1: TextStyle(
                fontSize: 36,
                color: BlogColors.header.color,
                fontWeight: FontWeight.bold),
            bodyText1: TextStyle(color: BlogColors.text.color)),
      ),
    );
  }
}
